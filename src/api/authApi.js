import axiosClient from './axiosClient';

const authApi = {
    login: (data) => {
        const url = '/QuanLyNguoiDung/DangNhap';
        return axiosClient.post(url, data);
    },
}

export default authApi;