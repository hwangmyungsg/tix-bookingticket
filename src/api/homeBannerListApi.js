import axiosClient from './axiosClient';

const homeBannerListApi = {
    getAll: () => {
        const url = 'https://5eea03d7b13d0a00164e40a1.mockapi.io/api/homeBannerList';
        return axiosClient.get(url);
    },
}

export default homeBannerListApi;