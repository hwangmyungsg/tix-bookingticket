import axiosClient from './axiosClient';

const MA_NHOM = process.env.REACT_APP_MA_NHOM;

const userApi = {
    getList: (params) => {
        const url = `/QuanLyNguoiDung/LayDanhSachNguoiDungPhanTrang?maNhom=${MA_NHOM}&${params}`;
        // return axiosClient.get(url, { params });
        return axiosClient.get(url);
    },

    deleteUser: (taiKhoan) => {
        const url = `/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`;
        return axiosClient.delete(url);
    }
}

export default userApi;