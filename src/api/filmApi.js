import axiosClient from './axiosClient';

const MA_NHOM = process.env.REACT_APP_MA_NHOM;

const filmApi = {
    getAll: (params) => {
        const url = '/QuanLyPhim/LayDanhSachPhim?maNhom='+MA_NHOM;
        return axiosClient.get(url, { params });
    },
}

export default filmApi;