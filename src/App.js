import React, { Suspense } from 'react';

// Route
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { routesHome, routesAdmin } from 'Routes';
import PageNotFound from 'Containers/PageNotFound';
import HomePageTemplate from 'Containers/Templates/HomePageTemplate';
import Auth from 'Containers/Auth';
import AdminTemplate from 'Containers/Templates/AdminTemplate';

function App() {
  // const routeList = (routes) => {
  //   if (routes && routes.length > 0) {
  //     return routes.map((item, index) => {
  //       return (
  //         <Route 
  //           key={index}
  //           exact={item.exact}
  //           path={item.path}
  //           component={item.component}
  //         />
  //       )
  //     });
  //   }
  // };

  const renderRoutesHomePage = (routes) => {
    if (routes && routes.length > 0) {
      return routes.map((item, index) => {
        return (
          // <Route 
          //   key={index}
          //   exact={item.exact}
          //   path={item.path}
          //   Component={item.component}
          // />
          <HomePageTemplate 
            key={index}
            exact={item.exact}
            path={item.path}
            Component={item.component}
          />
        )
      });
    }
  };

  const renderRoutesAdmin = (routes) => {
    if (routes && routes.length > 0) {
      return routes.map((item, index) => {
        return (
          <AdminTemplate 
            key={index}
            exact={item.exact}
            path={item.path}
            Component={item.component}
          />
        )
      });
    }
  };



  return (
    <Suspense fallback={<h1>Loading...</h1>}>
      <BrowserRouter>
        <Switch>
          { renderRoutesHomePage(routesHome) }
          { renderRoutesAdmin(routesAdmin) }
          {/* {routeList(routesHome)}
          {routeList(routesAdmin)} */}

          <Route path="/dang-nhap" exact component={Auth} />
          <Route path="" component={PageNotFound} />
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;