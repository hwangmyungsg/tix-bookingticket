import React from 'react'
import PropTypes from 'prop-types'

const Pagination = props => {
    const { pagination, onPageChange } = props;
    const { currentPage, totalPages } = pagination;
    // const totalPage = Math.ceil(totalCount / count);
    // 51 / 10 = 5.1 => 6

    function handlePageChange(newPage) {
        if (onPageChange) {
            onPageChange(newPage);
        }
    }

    return (
        <ul className="pagination m-0 float-right">
            <li className={`page-item ${(currentPage <= 1) ? 'disabled' : ''}`}>
                <button className="page-link"
                    disabled={currentPage <= 1}
                    onClick={() => handlePageChange(currentPage - 1)}
                >«</button>
            </li>
            {
                currentPage > 1 ?
                    <li className="page-item">
                        <button className="page-link"
                            onClick={() => handlePageChange(currentPage - 1)}
                        >{currentPage - 1}</button>
                    </li>
                    : ''
            }
            <li className="page-item active">
                <span className="page-link">{currentPage}</span>
            </li>
            {
                currentPage < totalPages ?
                    <li className="page-item">
                        <button className="page-link"
                            onClick={() => handlePageChange(currentPage + 1)}
                        >{currentPage + 1}</button>
                    </li>
                    : ''
            }
            <li className={`page-item ${(currentPage >= totalPages) ? 'disabled' : ''}`}>
                <button className="page-link"
                    disabled={currentPage >= totalPages}
                    onClick={() => handlePageChange(currentPage + 1)}
                >»</button>
            </li>
        </ul>
    )
}

Pagination.propTypes = {
    pagination: PropTypes.object.isRequired,
    onPageChange: PropTypes.func,
}

Pagination.defaultProps = {
    onPageChange: null,
}

export default Pagination