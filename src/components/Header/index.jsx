import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import HeaderUserLogged from './HeaderUserLogged';
import HeaderUserNotLogin from './HeaderUserNotLogin';

class Header extends PureComponent {
    render() {
        let { user }    = this.props;
        let isLoggedIn  = user.taiKhoan ? true : false;

        return (
            <header>
                <nav className="tix-navbar">
                    <a className="tix-brand" href="#hwangmyung"><img src="/assets/img/web-logo.png" width="50px" loading="lazy" alt="Website title" /></a>

                    <ul id="head-menu">
                        <li><a href="#hwangmyung">Lịch chiếu</a></li>
                        <li><a href="#hwangmyung">Cụm rạp</a></li>
                        <li><a href="#hwangmyung">Tin tức</a></li>
                        <li><a href="#hwangmyung">Ứng dụng</a></li>
                        <li><Link to="/dang-nhap">[ Login ]</Link></li>
                        <li><Link to="/@dmin">[ Admin ]</Link></li>
                    </ul>

                    <div className="nav-right">
                        <div className="nav-account">
                            { isLoggedIn ? <HeaderUserLogged/> : <HeaderUserNotLogin/> }
                        </div>
                    </div>

                    <div id="menu-dropdown">
                        {/* TODO: sideMenu */}
                        {/* <img src="/assets/img/icon/menu-options.png" onClick="setState_toggle_sideMenu(true)" alt="Side menu icon"/> */}
                        <input type="checkbox" id="toggle_sideMenu" />
                        <div id="sideMenu">
                            <div id="sideMenu__left">
                                <div id="sideMenu__right"></div>
                            </div>
                            <div id="sideMenu__back"></div>
                        </div>
                    </div>
                </nav>
            </header>
        );
    };
}

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.user,
    }
}

export default connect(mapStateToProps, null)(Header);