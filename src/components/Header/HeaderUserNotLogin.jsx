import React from 'react'
import { Link } from 'react-router-dom'

function HeaderUserNotLogin() {
    return (
        <>
            <Link to='/dang-nhap' className="nav-account__link">
                <img src="/assets/img/avatar.png" className="rounded-circle" alt="User avatar" />
                <span>Đăng nhập</span>
            </Link>
            <Link to='/dang-nhap' className="nav-account__link">
                <span>Đăng ký</span>
            </Link>
        </>
    )
}

export default HeaderUserNotLogin