import React from 'react'
import { connect } from 'react-redux';
import * as actions from 'Containers/Auth/modules/action';

function HeaderUserLogged(props) {
    let { user } = props;
    return (
        <>
            <div className="dropdown loggedIn">
                <button className="btn btn-default dropdown-toggle" type="button" id="dropdownLoggedIn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="/assets/img/avatar.png" className="rounded-circle" alt={ user.taiKhoan } />
                    { user.taiKhoan}
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownLoggedIn">
                    <a className="dropdown-item" href="#thongTinTaiKhoan">
                        Thông tin tài khoản
                    </a>
                    <button className="dropdown-item" type="button" onClick={() => props.logout()}>
                        Đăng xuất
                    </button>
                </div>
            </div>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.user,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(actions.actLogout());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderUserLogged);
