import React from 'react';

export default function Footer() {
    return (
        <footer>
            <div className="mainMaxWidth">
                <div className="row footer__links mx-0">
                    <div className="col-sm-4 col-6 footer__tix">
                        <p className="title hideOnMobile">TIX</p>
                        <div className="row">
                            <div className="col-sm-6">
                                <a href="#hwangmyung">FAQ</a>
                                <a href="#hwangmyung">Brand Guidelines</a>
                            </div>
                            <div className="col-sm-6">
                                <a href="#hwangmyung">Thỏa thuận sử dụng</a>
                                <a href="#hwangmyung">Chính sách bảo mật</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4 hideOnMobile">
                        <p className="title hideOnMobile">ĐỐI TÁC</p>
                        <div className="linePartner">
                            <a target="_blank" rel="noopener noreferrer" href="https://www.cgv.vn/" title="CGV">
                                <img className="bg-white" src="/assets/img/partner/cgv.png" alt="CGV logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://bhdstar.vn" title="BHD">
                                <img src="/assets/img/partner/bhd.png" alt="BHD logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://galaxycine.vn" title="Galaxy">
                                <img src="/assets/img/partner/galaxycine.png" alt="Galaxy logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://cinestar.com.vn" title="Cinestar">
                                <img src="/assets/img/partner/cinestar.png" alt="Cinestar logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://lottecinemavn.com" title="Lotte Cinema">
                                <img src="/assets/img/partner/lottecinemavn.png" alt="Lotte Cinema logo"/>
                            </a>
                        </div>
                        <div className="linePartner">
                            <a target="_blank" rel="noopener noreferrer" href="https://www.megagscinemas.vn" title="MegaGS">
                                <img className="bg-white" src="/assets/img/partner/megags.png" alt="MegaGS logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.betacineplex.vn/" title="Beta">
                                <img src="/assets/img/partner/bt.jpg" alt="Beta logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://ddcinema.vn" title="DDC">
                                <img src="/assets/img/partner/dongdacinema.png" alt="ĐC logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://touchcinema.com/" title="Touch Cinema">
                                <img src="/assets/img/partner/TOUCH.png" alt="Touch Cinema logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://cinemaxvn.com/" title="Cinemax">
                                <img src="/assets/img/partner/cnx.jpg" alt="Cinemax logo"/>
                            </a>
                        </div>
                        <div className="linePartner">
                            <a target="_blank" rel="noopener noreferrer" href="http://starlight.vn/" title="Starlight">
                                <img src="/assets/img/partner/STARLIGHT.png" alt="Starlight logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.dcine.vn/" title="Dcine">
                                <img src="/assets/img/partner/dcine.png" alt="Dcine logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://zalopay.vn/" title="ZaloPay">
                                <img src="/assets/img/partner/zalopay_icon.png" alt="Zalopay logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.payoo.vn/" title="Payoo">
                                <img src="/assets/img/partner/payoo.jpg" alt="Payoo logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.vietcombank.com.vn/" title="Vietcombank">
                                <img src="/assets/img/partner/VCB.png" alt="Vietcombank logo"/>
                            </a>
                        </div>
                        <div className="linePartner">
                            <a target="_blank" rel="noopener noreferrer" href="http://www.agribank.com.vn/" title="Agribank">
                                <img src="/assets/img/partner/AGRIBANK.png" alt="Agribank logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.vietinbank.vn/" title="Vietinbank">
                                <img src="/assets/img/partner/VIETTINBANK.png" alt="Vietinbank logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="https://www.indovinabank.com.vn/" title="IVB">
                                <img src="/assets/img/partner/IVB.png" alt="IVB logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://123go.vn" title="123Go">
                                <img src="/assets/img/partner/123go.png" alt="123Go logo"/>
                            </a>
                            <a target="_blank" rel="noopener noreferrer" href="http://laban.vn" title="La Bàn">
                                <img src="/assets/img/partner/laban.png" alt="La Bàn logo"/>
                            </a>
                        </div>
                    </div>
                    <div className="col-sm-4 col-6">
                        <div className="row">
                            <div className="col-sm-6 text-center hideOnMobile">
                                <p className="title">MOBILE APP</p>
                                <a target="_blank" rel="noopener noreferrer" href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197" title="Apple App">
                                    <img className="iconApp" src="/assets/img/icon/apple-logo.png" alt="Apple app"/>
                                </a>
                                <a target="_blank" rel="noopener noreferrer" href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123" title="Android App">
                                    <img className="iconApp" src="/assets/img/icon/android-logo.png" alt="Android app"/>
                                </a>
                            </div>
                            <div className="col-sm-6 text-center">
                                <p className="title hideOnMobile">SOCIAL</p>
                                <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/tix.vn/" title="Facebook social">
                                    <img className="iconApp" src="/assets/img/icon/facebook-logo.png" alt="Facebook logo"/>
                                </a>
                                <a target="_blank" rel="noopener noreferrer" href="https://zalo.me/tixdatve" title="Zalo social">
                                    <img className="iconApp" src="/assets/img/icon/zalo-logo.png" alt="Zalo logo"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <hr className="hrFooter" />

                <div className="row footer__info">
                    <div className="col-lg-1 col-xs-12 footer__logo">
                        <img src="/assets/img/icon/zion-logo.jpg" alt="Zion logo"/>
                    </div>
                    <div className="col-lg-9 text-lg-left col-xs-12 text-center px-3">
                        <span>TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</span>
                        <span>Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.</span>
                        <span>Giấy chứng nhận đăng ký kinh doanh số: 0101659783,<br />đăng ký thay đổi lần thứ 30,
                        ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư
                        Thành phố Hồ Chí Minh cấp.</span>
                        <span>Số Điện Thoại (Hotline): 1900 545 436<br />Email: <a href="mailto:support@tix.vn">support@tix.vn</a></span>
                    </div>
                    <div className="col-lg-2 col-xs-12 text-center">
                        <a target="_blank" rel="noopener noreferrer" href="http://online.gov.vn/Home/WebDetails/62782" className="float-lg-right text-center">
                            <img className="imgBoCongThuong" alt="Bộ Công Thương" src="/assets/img/icon/da-thong-bao-bo-cong-thuong.png" />
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    );
}