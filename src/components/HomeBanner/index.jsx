import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'Actions';
// Slick
import Slider from "react-slick";
import 'assets/vendor/slick-1.8.0/slick.css'

function PrevArrow(props) {
    const { onClick } = props;
    return (
        <button className='slick-arrow slick-prev' onClick={onClick}></button>
    );
}
function NextArrow(props) {
    const { onClick } = props;
    return (
        <button className='slick-arrow slick-next' onClick={onClick}></button>
    );
}

class HomeBanner extends Component {
    componentDidMount() {
        this.props.homeBannerList_fetch();
    }

    render() {
        var settings = {
            autoplay: true,
            autoplaySpeed: 6000,
            speed: 800,
            prevArrow: <PrevArrow/>,
            nextArrow: <NextArrow/>,
            dots: true,
            infinite: true,
        };
        return (
            <section className='homeBanner'>
                <Slider {...settings} className='homeBanner__content'>
                    <div className="homeBanner__item">
                        {/* TODO: link detailFilm */}
                        <a href="http://google.com" tabIndex={-1}>
                            <img src="/assets/img/banner/2020/06/con-mua-tinh-dau.png" alt="Movie title"/>
                            <div className="backgroundLinear" />
                            
                        </a>
                        <button className="playTrailer" type="button" data-toggle="modal" data-target="#trailerModal" onClick={() => this.props.setTrailer('https://www.youtube.com/watch?v=bwlWSoeU254')}/>
                    </div>
                    {/* <div className="homeBanner__item">
                        <a href="#hwangmyung" tabIndex={-1}>
                            <img src="/assets/img/banner/2020/06/skytour-15918637310942.jpg" alt="Movie title"/>
                            <div className="backgroundLinear" />
                            <button className="playTrailer venobox" type="button" tabIndex={-1} data-vbtype="video" data-autoplay="true" href="https://www.youtube.com/watch?v=t7m1iqs_b-U" />
                        </a>
                    <div className="homeBanner__item">
                        <a href="#hwangmyung" tabIndex={-1}>
                            <img src="/assets/img/banner/2020/06/sinh-nhat-chet-choc-2-15916767035273.jpg" alt="Movie title"/>
                            <div className="backgroundLinear" />
                            <button className="playTrailer venobox" type="button" tabIndex={-1} data-vbtype="video" data-autoplay="true" href="https://www.youtube.com/watch?v=ecYILs8MaQ4" />
                        </a>
                    </div> */}
                </Slider>



                {/* bookNow */}
                <div className="homeBanner__bookNow">
                    <div className="dropdown selectFilm">
                        <div className="dropdown-toggle" id="selectFilm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,0">
                            Phim
                        </div>
                        <div className="dropdown-menu" aria-labelledby="selectFilm">
                            <a className="dropdown-item" href="#hwangmyung">Action</a>
                            <a className="dropdown-item" href="#hwangmyung">Another action</a>
                            <a className="dropdown-item" href="#hwangmyung">Something else here</a>
                        </div>
                        </div>
                        <div className="dropdown selectCinema">
                        <div className="dropdown-toggle" id="selectFilm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,0">
                            Rạp
                        </div>
                        <div className="dropdown-menu" aria-labelledby="selectFilm">
                            <a className="dropdown-item" href="#hwangmyung">Action</a>
                            <a className="dropdown-item" href="#hwangmyung">Another action</a>
                            <a className="dropdown-item" href="#hwangmyung">Something else here</a>
                        </div>
                        </div>
                        <div className="dropdown selectDate">
                        <div className="dropdown-toggle" id="selectFilm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,0">
                            Ngày xem
                        </div>
                        <div className="dropdown-menu" aria-labelledby="selectFilm">
                            <a className="dropdown-item" href="#hwangmyung">Action</a>
                            <a className="dropdown-item" href="#hwangmyung">Another action</a>
                            <a className="dropdown-item" href="#hwangmyung">Something else here</a>
                        </div>
                        </div>
                        <div className="dropdown selectSession">
                        <div className="dropdown-toggle" id="selectFilm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,0">
                            Suất chiếu
                        </div>
                        <div className="dropdown-menu" aria-labelledby="selectFilm">
                            <a className="dropdown-item" href="#hwangmyung">Action</a>
                            <a className="dropdown-item" href="#hwangmyung">Another action</a>
                            <a className="dropdown-item" href="#hwangmyung">Something else here</a>
                        </div>
                    </div>
                    <button id="btnBuy">MUA VÉ NGAY</button>
                </div>

            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        trailer: state.trailer,
        homeBannerList: state.homeBannerList,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setTrailer: (trailerLink) => {
            dispatch(actions.setTrailer(trailerLink));
        },
        homeBannerList_fetch: () => {
            dispatch(actions.homeBannerList_fetch());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeBanner)