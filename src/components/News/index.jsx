import React from 'react'

export default function News() {
    return (
        <section className="news">
            <div className="mainMaxWidth news__content">
                <ul className="nav nav-tabs" role="tablist" id="news">
                    <li className="nav-item" role="presentation">
                        <a className="nav-link active" data-toggle="tab" href="#showingNews" role="tab" aria-selected="true">Điện Ảnh 24h</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="nav-link" data-toggle="tab" href="#showingReview" role="tab" aria-selected="false">Review</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="nav-link" data-toggle="tab" href="#showingPromotion" role="tab" aria-selected="false">Khuyến Mãi</a>
                    </li>
                </ul>



                <div className="tab-content" id="newsTabContent">
                    {/* NEWS */}
                    <div className="tab-pane fade show active clearfix" role="tabpanel" id="showingNews">
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/06/ban-dao-peninsula-la-bom-tan-xac-song-khong-the-bo-lo-15925398181587.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Bán Đảo Peninsula là bom tấn xác sống không thể bỏ lỡ!</p>
                            </a>
                            <p className="newsDescription">
                                Là phần phim khép lại bộ ba xác sống (Seoul Station, Train to Busan - 2016) của đạo diễn Yeon Sang Ho, mới đây, bom tấn Bán Đảo (Train to Busan 2/Peninsula) vừa chính thức tung trailer hé lộ những tình tiết mới cực hấp dẫn.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="wrapButtonSeeMoreNews">
                            <button className="btnViewMore">XEM THÊM</button>
                        </div>
                    </div>

                    {/* REVIEW */}
                    <div className="tab-pane fade clearfix" role="tabpanel" id="showingReview">
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/04/review-spider-man-into-the-spider-verse-15870356357626.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">Review: Spider-Man: Into The Spider-Vesre </p>
                            </a>
                            <p className="newsDescription">
                                Năm 2018 là một năm đầy thành công của nhân vật Người Nhện. Sau thành công của Spider-Man: Homecoming, “nhện nhọ” có màn hành động thuyết phục khán giả trong Avengers: Infinity War. Và rồi chúng ta lại có bộ phim hoạt hình đỉnh cao nhất của nhân vật "nhện nhọ".
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="wrapButtonSeeMoreNews">
                            <button className="btnViewMore">XEM THÊM</button>
                        </div>
                    </div>

                    {/* PROMOTION */}
                    <div className="tab-pane fade clearfix" role="tabpanel" id="showingPromotion">
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="news__item clearfix">
                            <div className="newsThumbnail">
                                <a href="#link">
                                    <img src="https://s3img.vcdn.vn/123phim/2020/05/cgv-ve-chi-59-000d-ca-tuan-15906628449549.png" alt="" />
                                </a>
                            </div>
                            <a href="#link">
                                <p className="newsTitle">CGV VÉ CHỈ 79.000Đ CẢ TUẦN!</p>
                            </a>
                            <p className="newsDescription">
                                Tận hưởng Ưu Đãi lên đến 3 VÉ CGV mỗi tuần chỉ với GIÁ 79.000Đ/VÉ khi trên TIX và thanh toán bằng ZaloPay.
                            </p>
                            <div className="blockIconFacebook">
                                <div className="wrapIcon">
                                    <img className="iconFacebook" alt="" src="/assets/img/icon/like.png" />
                                    <p className="amount like">0</p>
                                </div>
                                <a className="wrapIcon" href="#link">
                                    <img src="/assets/img/icon/comment.png" alt="" className="iconFacebook" />
                                    <p className="amount comment">0</p>
                                </a>
                            </div>
                        </div>
                        <div className="wrapButtonSeeMoreNews">
                            <button className="btnViewMore">XEM THÊM</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
