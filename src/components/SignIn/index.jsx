// import React from 'react'
// import { Formik, Form, Field, ErrorMessage } from 'formik';
// import { Link } from 'react-router-dom';
// import * as yup from 'yup';
// import signInSignUpApi from 'api/signInSignUpApi';

// export default function SignIn() {
//     const initialValues = {
//         taiKhoan: '',
//         matKhau: ''
//     }

//     const signInSchema = yup.object().shape({
//         taiKhoan:   yup.string().required('* Vui lòng nhập Tài khoản'),
//         matKhau:    yup.string().required('* Vui lòng nhập Mật khẩu'),
//     });

//     const handleSubmit = async (values) => {
//         console.log('submit: ', values);
        
//         try {
//             // console.log(resp);
//             const resp = await signInSignUpApi.signIn(values);
//             localStorage.setItem('user', JSON.stringify(resp));
//         } catch (error) {
//             console.log('Failed to fetch film list: ', error);
//         }
//     };

//     return (
//         <section className="signin">
//             <div className="signin__wrapper">
//                 <div className="header">
//                     <img src="./assets/img/icon/fade-loading.png" alt="Logo" className="header__logo" />
//                     <h1>ĐĂNG NHẬP</h1>
//                 </div>

//                 <Formik
//                     initialValues={initialValues}
//                     validationSchema={signInSchema}
//                     onSubmit={(values, { setSubmitting }) => {
//                         // setTimeout(() => {
//                         //     alert(JSON.stringify(values, null, 2));
//                         //     setSubmitting(false);
//                         //     handleSubmit(values);
//                         // }, 1000);

//                         handleSubmit(values);
//                         setSubmitting(false);
//                     }}
//                 >
//                     {(formikProps) => (
//                         <Form>
//                             <div className="form-group">
//                                 <Field type="text" className="form-control" placeholder="Tài khoản" name="taiKhoan" onChange={formikProps.handleChange} />
//                                 <ErrorMessage name="taiKhoan" />
//                             </div>
//                             <div className="form-group">
//                                 <Field type="password" className="form-control" placeholder="Mật khẩu" name="matKhau" onChange={formikProps.handleChange} />
//                                 <ErrorMessage name="matKhau" />
//                             </div>
//                             <div className="form-group">
//                                 <button type="submit" className="btn btn-lg btn-primary btn-block" disabled={formikProps.isSubmitting}>Đăng nhập</button>
//                             </div>
//                             <div className="form-group text-center">
//                                 <a href="signup.html" className="pull-right text__signUp">
//                                     Chưa có tài khoản? <strong>Đăng ký</strong>
//                                 </a>
//                             </div>
//                         </Form>
//                     )}
//                 </Formik>

//                 {/* Back to Home */}
//                 <Link to='/'><div className="close"></div></Link>
//             </div>
//         </section>
//     );
// }
