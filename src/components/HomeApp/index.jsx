import React from 'react';
// Slick
import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";

export default function HomeApp() {
    let settings = {
        autoplay: true,
        infinite: true,
        dots: false,
        arrows: false,
    };

    return (
        <section className="homeApp">
            <div className="mainMaxWidth">
                <div className="row mx-0">
                    <div className="col-md-6 left">
                        <div className="left__content text-center text-md-left">
                            <p className="textLeft">Ứng dụng tiện lợi dành cho</p>
                            <p className="textLeft">người yêu điện ảnh</p>
                            <br />
                            <p className="textSmallLeft">Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</p>
                            <br />
                            <a className="buttonLeft" href="https://itunes.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197?mt=8" target="_blank" rel="noopener noreferrer">App miễn phí - Tải về ngay!</a>
                            <p className="textAppUnder">
                                TIX có hai phiên bản
                                <a className="tagA" target="_blank" href="https://itunes.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197?mt=8" rel="noopener noreferrer">iOS</a>
                                & 
                                <a className="tagA" target="_blank" href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123" rel="noopener noreferrer">Android</a>
                            </p>
                        </div>
                    </div>
                    <div className="col-md-6 right">
                        <img className="img-responsive phone-img" src="/assets/img/icon/mobile.png" alt="Loading..." />
                        <Slider {...settings} className='homeApp__slides'>
                            <img src="/assets/img/slide/slide1.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide2.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide3.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide4.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide5.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide6.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide7.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide8.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide9.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide10.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide11.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide12.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide13.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide14.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide15.jpg" alt="Slide img"/>
                            <img src="/assets/img/slide/slide16.jpg" alt="Slide img" />
                        </Slider>
                    </div>
                </div>
            </div>
        </section>
    );
}
