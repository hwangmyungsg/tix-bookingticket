import React, { Component } from 'react';
import * as actions from 'Actions';
import { connect } from 'react-redux';
import $ from 'jquery';

class ModalTrailer extends Component {
    componentDidMount() {
        let app     = this;
        let modal   = document.getElementById('trailerModal');
        
        // click outside modal
        window.onclick = function(event) {
            if (event.target === modal) {
                app.props.unsetTrailer();
                $('#trailerModal').modal('hide');
            }
        }
    }

    render() {
        return (
            <div className="modal fade trailerModal" id="trailerModal" tabIndex={-1} role="dialog" aria-labelledby="trailerModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button type="button" className="close closeBtn" data-dismiss="modal" onClick={() => this.props.unsetTrailer() }/>
                            <iframe id="iframeTrailerByYoutube" title="iframeTrailerByYoutube" src={this.props.trailer} frameBorder={0} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        trailer: state.trailer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        unsetTrailer: () => {
            dispatch(actions.unsetTrailer());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalTrailer);