import React from 'react';
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'

function Loading({loading, children}) {
    return (
        // <div className="loading">
        //     <div className="loading__content">
        //         <div className="revolver">
        //             <section className="revolver__part" />
        //             <section className="revolver__part" />
        //             <section className="revolver__part" />
        //             <section className="revolver__part" />
        //             <section className="revolver__part" />
        //             <section className="revolver__part" />
        //         </div>
        //     </div>
        // </div>
        <LoadingOverlay
            active={loading}
            spinner={<BounceLoader />}
        >
            {children}
        </LoadingOverlay>
    )
}
export default Loading;