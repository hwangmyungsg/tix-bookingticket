import * as types from 'Constants/actionType';
import homeBannerListApi from 'api/homeBannerListApi';

let initialState = [];

let homeBannerListReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.HOMEPAGE_HOMEBANNERLIST_FETCH:
            const homeBannerList_fetch = async () => {
                try {
                    const resp = await homeBannerListApi.getAll();
                    console.log('homeBannerList / fetched');
                    return resp;
                } catch (error) {
                    console.log('Failed to fetch homeBannerList: ', error);
                }
            }
            state = homeBannerList_fetch();
            return state;
        
            // try {
            //     const resp = homeBannerListApi.getAll();
            //     console.log('homeBannerList / fetched');
            //     return resp;
            // } catch (error) {
            //     console.log('Failed to fetch homeBannerList: ', error);
            // }
    
        default:
            return state;
    }
}

export default homeBannerListReducer;