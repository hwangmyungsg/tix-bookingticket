import { combineReducers } from 'redux';
// import tasks from './task';
import trailer from 'Reducers/TrailerReducer';
import homeBannerList from 'Reducers/HomeBannerListReducer';

import authReducer from 'Containers/Auth/modules/reducer';
import userReducer from 'Containers/Admin/Users/modules/reducer'

const rootReducers= combineReducers({
    // tasks,           // tasks: tasks
    trailer,
    homeBannerList,

    // Auth
    authReducer,
    userReducer,
});

export default rootReducers;