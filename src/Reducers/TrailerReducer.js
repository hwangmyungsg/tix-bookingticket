import * as types from 'Constants/actionType';
import * as helpers from 'helpers';

let initialState = '';

let trailerReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.HOMEPAGE_TRAILER_SET:
            let youtubeId = helpers.getYoutubeId(action.linkTrailer);
            state = `https://www.youtube.com/embed/${youtubeId}?autoplay=1`;
            return state;

        case types.HOMEPAGE_TRAILER_UNSET:
            state = '';
            return state;
    
        default:
            return state;
    }
}

export default trailerReducer;