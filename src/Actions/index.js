import * as types from 'Constants/actionType';

export const setTrailer = (linkTrailer) => {
    return {
        type: types.HOMEPAGE_TRAILER_SET,
        linkTrailer
    }
}

export const unsetTrailer = () => {
    return {
        type: types.HOMEPAGE_TRAILER_UNSET,
    }
}

export const homeBannerList_fetch = () => {
    return {
        type: types.HOMEPAGE_HOMEBANNERLIST_FETCH,
    }
}