import React from 'react'
import { Link, Redirect, Route } from 'react-router-dom';
import Navbar from 'Containers/Admin/Navbar';
import LeftSidebar from 'Containers/Admin/LeftSidebar';
import Footer from 'Containers/Admin/Footer';
import { connect } from 'react-redux';

// Style
import 'assets/fonts/fontawesome-free/css/all.min.css';
import 'assets/vendor/bootstrap-4.5.0-dist/css/bootstrap.min.css';
// import 'assets/admin/css/googleFont_SansPro.css';
import 'assets/admin/css/adminlte.min.css';
import 'assets/admin/css/adminlte_custom.css';
// import { ContentWrapper } from 'Containers/Admin/ContentWrapper';

function AdminLayout(props) {
    return (
        // <div>
        //     <h1>Navbar Admin</h1>
        //     <div>Sidebar</div>
        //     {/* CONTENT */}
        //     {props.children}
        // </div>

        <div className="wrapper">
            <Navbar />

            <LeftSidebar />

            {/* Container Wrapper */}
            <div className="content-wrapper">
                {/* <ContentWrapper /> */}
                {props.children}
            </div>{/* /.container-wrapper */}

            <Footer />
        </div>
    )
}

function AdminTemplate({ Component, ...props }) {
    return (
        <Route
            {...props}
            render={propsComponent => {
                if (localStorage.getItem('user')) {
                    // Check role
                    if (JSON.parse(localStorage.getItem('user'))['maLoaiNguoiDung'] === 'QuanTri') {
                        return (
                            <AdminLayout>
                                <Component {...propsComponent} />
                            </AdminLayout>
                        );
                    } else {
                        return (
                            // TODO: layout Deny access Admin
                            <>
                                <h1>
                                    Bạn không có quyền truy cập vào đây! <br />
                                    <Link to='/'>[Trở về Trang chủ]</Link>
                                </h1>
                                
                            </>
                        )
                    }
                }

                return <Redirect to='/dang-nhap' />;
            }}
        />
    )
}

let mapStateToProps = (state) => {
    return {
        user: state.authReducer.user,
    }
}

export default connect(mapStateToProps, null)(AdminTemplate)