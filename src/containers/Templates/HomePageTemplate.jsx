import React from 'react'
import { Route } from 'react-router-dom'

function HomePageTemplate({ Component, ...props }) {
    return (
        <Route
            {...props}
            render={(propsComponent) => (
                <Component { ...propsComponent }/>
            )}
        />
    )
}

export default HomePageTemplate