import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from 'Containers/Auth/modules/action'

function Navbar(props) {

    const { user, updateUser } = props;
    useEffect(() => {
        let userLocalStorage = JSON.parse(localStorage.getItem('user'));
        if (userLocalStorage && userLocalStorage.maLoaiNguoiDung === 'QuanTri') {
            updateUser(userLocalStorage);
        }
    }, [updateUser]);

    return (
        <nav className="main-header navbar navbar-expand navbar-dark navbar-secondary">
            {/* Left navbar links */}
            <ul className="navbar-nav">
                <li className="nav-item">
                    <a className="nav-link" data-widget="pushmenu" href="#leftSidebar" role="button"><i className="fas fa-bars" /></a>
                </li>
                <li className="nav-item d-none d-sm-inline-block">
                    <Link className='nav-link' to='/'>Trang chủ</Link>
                </li>
            </ul>



            {/* Right navbar links */}
            <ul className="navbar-nav ml-auto">
                {/* Notifications Dropdown Menu */}
                <li className="nav-item dropdown">
                    <a className="nav-link admin-dropdown" data-toggle="dropdown" href="#dropdown_navbar">
                        {/* <i className="far fa-user" /> */}
                        {/* <span className="badge badge-warning navbar-badge">15</span> */}
                        <img src="/assets/img/avatar.png" className="rounded-circle admin-avatar" alt={user.taiKhoan} />
                        { user.taiKhoan } 
                    </a>
                    <div className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        {/* <span className="dropdown-item dropdown-header">15 Notifications</span> */}
                        {/* <div className="dropdown-divider" /> */}
                        <a href="#thongTinTaiKhoan" className="dropdown-item">
                        <i className="fas fa-user mr-2" /> Thông tin tài khoản
                        </a>
                        <div className="dropdown-divider" />
                        <button className="dropdown-item" onClick={() => props.logout()}>
                            <i className="fas fa-sign-out-alt mr-2" /> Đăng xuất
                        </button>
                    </div>
                </li>
            </ul>
        </nav>
    )
}

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.user,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateUser: (user) => {
            dispatch(actions.actLoginSuccess(user));
        },
        logout: () => {
            dispatch(actions.actLogout());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)