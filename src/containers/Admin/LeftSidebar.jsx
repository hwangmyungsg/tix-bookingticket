import React from 'react'
import { Link, NavLink } from 'react-router-dom'

function LeftSidebar() {
    return (
        <aside className="main-sidebar sidebar-dark-primary elevation-4">
            {/* Brand Logo */}
            <Link className="brand-link" to='/'>
                <img src="/assets/img/icon/fade-loading.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{ opacity: '.8' }} />
                <span className="brand-text font-weight-light">TIX ADMIN</span>
            </Link>



            {/* Sidebar */}
            <div className="sidebar">
                {/* Sidebar Menu */}
                <nav className="mt-2">
                    {/* nav-child-indent */}
                    <ul className="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                        {/* menu-open */}
                        {/* <li className="nav-item has-treeview menu-open">
                            <a href="#dashboard" className="nav-link">
                                <i className="nav-icon fas fa-tachometer-alt" />
                                <p>
                                    Dashboard <i className="right fas fa-angle-left" />
                                </p>
                            </a>
                            <ul className="nav nav-treeview">
                                <li className="nav-item">
                                    <a href="../../index.html" className="nav-link active">
                                        <i className="far fa-circle nav-icon" />
                                        <p>Dashboard v1</p>
                                    </a>
                                </li>
                            </ul>
                        </li> */}
                        

                        <li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/@dmin" exact>
                                <i className="nav-icon fas fa-tachometer-alt" />
                                <p>Dashboard</p>
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" activeClassName="active" to="/@dmin/users">
                                <i className="nav-icon fas fa-user" />
                                <p>Users</p>
                            </NavLink>
                        </li>

                    </ul>
                </nav>{/* /.sidebar-menu */}
            </div>{/* /.sidebar */}
        </aside>
    )
}

export default LeftSidebar
