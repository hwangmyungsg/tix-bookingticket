import React from 'react'
import { Link } from 'react-router-dom'

function AdminDashboard(props) {
    return (
        <>
            {/* Content Header (Page header) */}
            <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1>Dashboard</h1>
                        </div>
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-sm-right">
                                <li className="breadcrumb-item active"><Link to="/@dmin">Dashboard</Link></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>



            {/* Main content */}
            <section className="content">
                <div className="container-fluid">
                    <div className="card card-primary card-outline">
                        <div className="card-header">
                            <h3 className="card-title">Welcome back, have a nice day!</h3>
                        </div>
                        {/* /.card-body */}
                        <div className="card-body">
                            <img src="https://source.unsplash.com/weekly?movie" alt="" style={{ 'height': '300px' }}/>
                        </div>{/* /.card-body */}
                    </div>
                </div>{/* /.container-fluid */}
            </section>

        </>
    )
}

export default AdminDashboard