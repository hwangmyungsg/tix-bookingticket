import * as types from './constant';
let initialState = {
    loading: false,
    error: null,
    userList: {
        items: [],
        currentPage: 1,
        count: 25,
        totalCount: 1,
        totalPages: 1,
    },
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.USER_USERLIST_REQUEST:
            state.loading   = true;
            state.error     = {};
            // state.userList  = initialState.userList;
            return { ...state };
        
        case types.USER_USERLIST_SUCCESS:
            state.loading   = false;
            state.error     = {};
            state.userList  = action.data;
            return { ...state };
        
        case types.USER_USERLIST_FAILED:
            state.loading   = false;
            state.error     = action.error;
            // state.userList  = initialState.userList;
            return { ...state };
        
        default:
            return { ...state };
    }
}

export default userReducer