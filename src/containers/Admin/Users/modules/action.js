import userApi from 'api/userApi';
import * as types from './constant';

export const fetchUserList = (paramString) => {
    return dispatch => {
        dispatch(userListRequest());

        userApi.getList(paramString)
            .then(resp => {
                dispatch(userListSuccess(resp));
            })
            .catch(err => {
                dispatch(userListFailed(err));
            });
    }
}

export const userListRequest = () => {
    return {
        type: types.USER_USERLIST_REQUEST,
    }
}
export const userListSuccess = (data) => {
    return {
        type: types.USER_USERLIST_SUCCESS,
        data
    }
}
export const userListFailed = (err) => {
    return {
        type: types.USER_USERLIST_FAILED,
        err
    }
}