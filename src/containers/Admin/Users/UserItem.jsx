import React from 'react'

export default function UserItem(props) {
    const { user, index } = props;
    return (
        <tr>
            <td>{ index+1 }</td>
            <td className='text-bold'>{ user.taiKhoan }</td>
            <td>{ user.hoTen }</td>
            <td>{ user.email }</td>
            <td>{ user.soDt }</td>
            <td>
                <span className={`badge ${(user.maLoaiNguoiDung === 'QuanTri') ? 'bg-danger' : 'bg-green'} `}
                >{user.maLoaiNguoiDung}</span>
            </td>
            <td className="text-center">
                <div className="btn-group">
                    <button type="button" className="btn btn-default" title="Edit"
                        onClick={() => props.handleEditUser(user.taiKhoan)}
                    ><i className="fas fa-edit"></i></button>
                    <button type="button" className="btn btn-default" title="Delete"
                        onClick={() => props.handleDeleteUser(user.taiKhoan)}
                    ><i className="fas fa-trash-alt"></i></button>
                </div>
            </td>
        </tr>
    )
}