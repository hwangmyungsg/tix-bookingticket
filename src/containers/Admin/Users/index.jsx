import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import * as actions from './modules/action'
import queryString from 'query-string'
import { Link } from 'react-router-dom';
// import userApi from 'api/userApi';
import _ from 'lodash';
import Swal from 'sweetalert2';

// Component
import Pagination from 'Components/Pagination';
import UserItem from './UserItem';
import userApi from 'api/userApi';
import { Button } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import Modal from './Modal';

const Users = (props) => {
    const { userList, fetchUserList } = props;
    // const [pagination, setPagination] = useState({
    //     items: [],
    //     currentPage: 1,
    //     count: 25,
    //     totalCount: 1,
    //     totalPages: 1,
    // });
    const [filters, setFilters] = useState({
        soPhanTuTrenTrang: 10,
        soTrang: 1,
        tuKhoa: '',
    })

    // Modal
    const [openModal, setOpenModal] = useState(false);
    const [titleModal, setTitleModal] = useState('Add User');

    const delayedQuery = _.debounce(keyword => {
        setFilters({
            ...filters,
            tuKhoa: keyword,
            soTrang: 1,
        });
    }, 500);

    function handlePageChange(newPage) {
        setFilters({
            ...filters,
            soTrang: newPage,
        });
    }

    function handleLimitPageChange(e) {
        let litmit = e.target.value;
        setFilters({
            ...filters,
            soPhanTuTrenTrang: litmit,
            soTrang: 1,
        });
    }

    function handleSearchChange(e) {
        let keyword = e.target.value;
        delayedQuery(keyword);
    }

    function handleDeleteUser(taiKhoan) {
        Swal.fire({
            title: `<span>Bạn có muốn xóa tài khoản:<br/><strong>${taiKhoan}</strong></span>`,
            confirmButtonText: `Xóa`,
            showCancelButton: true,
            cancelButtonText: `Hủy`,
            // focusCancel: true,
            customClass: {
                confirmButton: 'swal2-deny swal2-styled',
            },
        }).then((result) => {
            if (result.isConfirmed) {
                userApi.deleteUser(taiKhoan)
                    .then(resp => {
                        console.log(`Xóa user thành công: ${taiKhoan}`);
                        Swal.fire({
                            icon: 'success',
                            title: `Xóa thành công!`,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        fetchUserList(filters);
                    })
                    .catch(err => {
                        console.log(`Error: ${err}`);
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: `Không thể xóa! Vui lòng thử lại<br/>${err}`,
                        })
                    });
            }
        })
    }

    // TODO: load user
    function handleEditUser(taiKhoan) {
        console.log('Edit user:', taiKhoan);
    }

    const renderUsersList = () => {
        const users = userList.items;
        if (users.length > 0) {
            return users.map((item, index) => {
                return <UserItem key={index} index={index} user={item}
                    handleEditUser={handleEditUser}
                    handleDeleteUser={handleDeleteUser}
                />
            });
        } else {
            return (
                <tr><td colSpan="7">Không có dữ liệu</td></tr>
            )
        }
    }



    useEffect(() => {
        fetchUserList(filters);
    }, [filters, fetchUserList]);
    // eslint-disable-line react-hooks/exhaustive-deps


    // Render
    return (
        <>
            {/* Content Header (Page header) */}
            <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1>Quản lý User</h1>
                        </div>
                        <div className="col-sm-6">
                            <ol className="breadcrumb float-sm-right">
                                <li className="breadcrumb-item"><Link to="/@dmin">Dashboard</Link></li>
                                <li className="breadcrumb-item active">Quản lý User</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>{/* /.content-header */}

            {/* Main content */}
            <section className="content">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-header">
                                    {/* <h3 className="card-title">User list</h3> */}
                                    <Button variant="outlined" color="primary" disableElevation startIcon={<Add />} onClick={() => setOpenModal(true)}>
                                        THÊM
                                    </Button>
                                    <div className="card-tools">
                                        <div className="input-group" style={{ width: '100%' }}>
                                            <input type="text" className="form-control float-right" placeholder="Tìm kiếm" onChange={handleSearchChange} autoComplete="off" />
                                            <div className="input-group-append">
                                                <span className="input-group-text"><i className="fas fa-search" /></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>{/* /.card-header */}
                                <div className="card-body table-responsive p-0">
                                    <table className="table table-hover text-nowrap table-td-middle">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tài khoản</th>
                                                <th>Họ tên</th>
                                                <th>Email</th>
                                                <th>Số ĐT</th>
                                                <th>Loại người dùng</th>
                                                <th className="text-center"><i className="fas fa-cogs"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { renderUsersList() }
                                        </tbody>
                                    </table>
                                </div>{/* /.card-body */}
                                <div className="card-footer p-pagination">
                                    <div className="row">
                                        <div className="col-sm-4 d-flex--align-center">
                                            Total: { userList.totalCount }
                                        </div>
                                        <div className="col-sm-4 d-flex--align-center">
                                            <span className="pr-5px">Rows: </span>
                                            <select className="form-control" style={{ width: 'auto' }} onChange={(e) => handleLimitPageChange(e)}>
                                                <option>10</option>
                                                <option>25</option>
                                                <option>50</option>
                                                <option>100</option>
                                            </select>
                                        </div>
                                        <div className="col-sm-4">
                                            <Pagination
                                                pagination={userList}
                                                onPageChange={handlePageChange}
                                            />
                                        </div>
                                    </div>
                                </div>{/* /.card-footer */}
                            </div>{/* /.card */}
                        </div>
                    </div>
                </div>
            </section>{/* /.content */}
        
            {/* Modal */}
            <Modal open={openModal} setOpen={setOpenModal} title={titleModal}>
                ákldjlsadjlsajdlskajdklj
            </Modal>
        </>
    )
}

const mapStateToProps = state => ({
    loading:    state.userReducer.loading,
    error:      state.userReducer.error,
    userList:   state.userReducer.userList,
})

const mapDispatchToProps = dispatch => {
    return {
        fetchUserList: (filters) => {
            // const paramString   = queryString.stringify(filters);
            // FIX lỗi API fetchUser không hỗ trợ param tuKhoa = ''
            let filtersTemp = { ...filters };
            if (filtersTemp['tuKhoa'] === '') {
                delete filtersTemp['tuKhoa'];
            }
            const paramString   = queryString.stringify(filtersTemp);
            dispatch(actions.fetchUserList(paramString));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users)