import React, { useEffect, useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { Link } from 'react-router-dom';
import * as yup from 'yup';
import { connect } from 'react-redux';
import * as actions from './modules/action';
import * as actions_HomePage from 'Containers/HomePage/modules/action';

function Auth(props) {
    const [isLoading, setIsLoading] = useState(true);

    const initialValues = {
        taiKhoan: '',
        matKhau: ''
    }

    const signInSchema = yup.object().shape({
        taiKhoan:   yup.string().required('* Vui lòng nhập Tài khoản'),
        matKhau:    yup.string().required('* Vui lòng nhập Mật khẩu'),
    });

    const handleSubmit = async (values) => {
        props.login(values, props.history);
    };

    const renderNoti = () => {
        const { error } = props;
        if (error) {
            return <div className="alert alert-danger" role="alert">{error}</div>
        }
    }

    const { updateUser, history } = props;
    useEffect(() => {
        const user = JSON.parse(localStorage.getItem('user'));
        if (user && user.maLoaiNguoiDung === 'QuanTri') {
            updateUser(user);
            history.push('/@dmin');
        } else if (user && user.maLoaiNguoiDung === 'KhachHang') {
            updateUser(user);
            history.push('/');
        }
        // isLoading = false;
        setIsLoading(false);
    }, [updateUser, history]);

    if (isLoading === false) {
        return (
            <>
                {/* { renderLoading() } */}
                <section className="signin">
                    <div className="signin__wrapper">
                        <div className="header">
                            <img src="assets/img/icon/fade-loading.png" alt="Logo" className="header__logo" />
                            <h1>ĐĂNG NHẬP</h1>
                        </div>
    
                        <Formik
                            initialValues={initialValues}
                            validationSchema={signInSchema}
                            onSubmit={(values, { setSubmitting }) => {
                                setSubmitting(true);
                                handleSubmit(values)
                                    .then(() => {
                                        setSubmitting(false);
                                    });
                            }}
                        >
                            {(formikProps) => (
                                <Form>
                                    <div className="form-group">
                                        <Field type="text" className="form-control" placeholder="Tài khoản" name="taiKhoan" onChange={formikProps.handleChange} />
                                        <ErrorMessage name="taiKhoan" />
                                    </div>
                                    <div className="form-group">
                                        <Field type="password" className="form-control" placeholder="Mật khẩu" name="matKhau" onChange={formikProps.handleChange} />
                                        <ErrorMessage name="matKhau" />
                                    </div>
    
                                    { renderNoti() }
    
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-lg btn-primary btn-block" disabled={formikProps.isSubmitting}>Đăng nhập</button>
                                    </div>
                                    <div className="form-group text-center">
                                        <a href="signup.html" className="pull-right text__signUp">
                                            Chưa có tài khoản? <strong>Đăng ký</strong>
                                        </a>
                                    </div>
                                </Form>
                            )}
                        </Formik>
    
                        {/* Back to Home */}
                        <Link to='/'><div className="close"></div></Link>
                    </div>
                </section>
            </>
        )
    } else {
        return (<></>)
    }
}

const mapStateToProps = state => {
    return {
        loading:    state.authReducer.loading,
        error:      state.authReducer.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        login: (user, history) => {
            dispatch(actions.actLogin_fetch(user, history));
        },
        updateUser: (user) => {
            dispatch(actions_HomePage.updateUser(user));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);