import authApi from 'api/authApi';
import * as types from './constant';

export const actLogin_fetch = (user, history) => {
    return dispatch => {
        dispatch(actLoginRequest());

        authApi.login(user)
            .then(resp => {
                const user = resp;
                // SET default header Authorization accessToken
                user.accessToken = `Bearer ${user.accessToken}`;
                localStorage.setItem('user', JSON.stringify(user));
                dispatch(actLoginSuccess(user));
                if (user.maLoaiNguoiDung === 'QuanTri') {
                    // Chuyển đến Admin
                    history.push('/@dmin');
                } else {
                    // Chuyển đến HomePage
                    history.push('/');
                }
            })
            .catch(err => {
                console.log(err);
                dispatch(actLoginFailed('Tài khoản hoặc Mật khẩu không đúng!'));
            })
    }
}

export const actLogout = () => {
    return {
        type: types.AUTH_LOGOUT,
    }
}

const actLoginRequest = () => {
    return {
        type: types.AUTH_REQUEST,
    }
}

export const actLoginSuccess = (data) => {
    return {
        type: types.AUTH_SUCCESS,
        data
    }
}

const actLoginFailed = (error) => {
    return {
        type: types.AUTH_FAILED,
        error
    }
}