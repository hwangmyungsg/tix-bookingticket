export const AUTH_REQUEST   = 'AUTH/AUTH_REQUEST';
export const AUTH_SUCCESS   = 'AUTH/AUTH_SUCCESS';
export const AUTH_FAILED    = 'AUTH/AUTH_FAILED';
export const AUTH_LOGOUT    = 'AUTH/LOGOUT';