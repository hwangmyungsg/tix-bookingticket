import axiosClient from 'api/axiosClient';
import * as types from './constant';

let initialState = {
    loading: false,
    user: {},
    error: null,
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.AUTH_REQUEST:
            state.loading   = true;
            state.user      = {};
            state.error     = null;
            return { ...state };
        
        case types.AUTH_SUCCESS:
            state.loading   = false;
            state.user      = action.data;
            if (axiosClient.defaults.headers.common['Authorization'] === undefined) {
                axiosClient.defaults.headers.common['Authorization'] = state.user.accessToken;
            }
            state.error     = null;
            return { ...state };
        
        case types.AUTH_FAILED:
            state.loading   = false;
            state.user      = {};
            state.error     = action.error;
            return { ...state };
        
        case types.AUTH_LOGOUT:
            localStorage.removeItem('user');
            state.user      = {};
            return { ...state };
    
        default:
            return { ...state };
    }
}

export default authReducer;