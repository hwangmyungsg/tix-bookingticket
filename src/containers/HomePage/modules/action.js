import { AUTH_SUCCESS } from 'Containers/Auth/modules/constant';

export const updateUser = (user) => {
    return {
        type: AUTH_SUCCESS, 
        data: user,
    }
}