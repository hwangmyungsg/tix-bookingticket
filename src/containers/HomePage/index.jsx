import React, {useEffect} from 'react';
import 'assets/sass/main.scss';
import Header from 'Components/Header';
import HomeBanner from 'Components/HomeBanner';
import Footer from 'Components/Footer';
import HomeApp from 'Components/HomeApp';
import News from 'Components/News';
import ModalTrailer from 'Components/ModalTrailer';
import { connect } from 'react-redux';
import * as actions from './modules/action';
// import filmApi from 'api/filmApi';

function Homepage(props) {
    // const [filmList, setFilmList] = useState([]);

    const { updateUser } = props;

    useEffect(() => {
        const user = localStorage.getItem('user');
        if (user !== null) {
            updateUser(JSON.parse(user));
        }

        // const fetchFilmList = async () => {
        //     try {
        //         const resp = await filmApi.getAll();
        //         console.log(resp);
        //     } catch (error) {
        //         console.log('Failed to fetch film list: ', error);
        //     }
        // }

        // fetchFilmList();
    }, [updateUser]);

    return (
        <>
            <Header />

            <HomeBanner />
            <News/>
            <HomeApp />
            
            <Footer />
            <ModalTrailer />
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.user,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateUser: (user) => {
            dispatch(actions.updateUser(user));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);