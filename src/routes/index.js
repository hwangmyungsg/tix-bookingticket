import HomePage from 'Containers/HomePage';
import AdminDashboard from 'Containers/Admin/AdminDashboard';
import Users from 'Containers/Admin/Users';

const routesHome = [
    {
        exact:      true,
        path:       '/',
        component:  HomePage,
    },
    // Detail
];


const routesAdmin_prefix = '/@dmin';
const routesAdmin = [
    {
        exact:      true,
        path:       routesAdmin_prefix,
        component:  AdminDashboard,
    },
    {
        exact:      true,
        path:       routesAdmin_prefix+'/users',
        component:  Users,
    },
];

export { routesHome, routesAdmin };