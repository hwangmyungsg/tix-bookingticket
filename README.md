# TIX - Booking ticket
### `CYBERSOFT` `FE42` `Nguyễn Hoàng Minh`

## Requirements:

# `1. HOME`

### 1.1. Trang chủ
- Lấy danh danh sách phim, dùng API (1)
- Danh sách hệ thống rạp phim:
	+ Cụm rạp
	+ Danh sách phim của cụm rạp đang chiếu

### 1.2. Chi tiết phòng vé

### 1.3. Chi tiết đặt vé

### 1.4. Đăng ký

### 1.5. Đăng nhập

### 1.6. Thông tin cá nhân

### 1.7. Thông tin đặt vé





# `2. ADMIN`
### 2.1. Quản lý lịch chiếu: CRUD
### 2.2. Quản lý phim: CRUD
### 2.3. Quản lý người dùng: CRUD

<!-- 
## Available Scripts

In the project directory, you can run:


### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**
## Learn More
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
### Deployment
### `npm run build` fails to minify
-->


<!--
Package:
- react-slick
https://react-slick.neostack.com/docs/get-started/
-->